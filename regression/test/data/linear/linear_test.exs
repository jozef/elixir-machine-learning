defmodule RegressionLinearTest do
  use ExUnit.Case
  doctest Regression.Linear

  test "calculates regression" do
    {intercept, slope, rss} = Regression.Linear.new("test/data/kc_house_train_data.csv", "test/data/kc_house_test_data.csv", ["sqft_living"], "price")
    assert intercept == -47116.07907289418
    assert slope == 281.9588396303426
    assert rss == 1201918354177286.3
  end

end
