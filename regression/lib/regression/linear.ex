defmodule Regression.Linear do
  alias Regression
  @moduledoc """
  Documentation for Regression.
  """

  @doc """
  Calculates linear regression.

  ## Examples

      iex> Regression.Linear.new("test/data/kc_house_sample_data.csv", "test/data/kc_house_sample_data.csv", ["sqft_living"], "price")
      {-13816.056604250334, 225.66344899088656, 105046133759.27667}

  """
  def new(train_data_name, test_data_name, feature_names, output_name) do
    columns = [output_name] ++ feature_names
    {output, features} = Regression.load_data(train_data_name, columns)
    feature = features |> Enum.at(0)
    {intercept, slope} = linear_regression(feature, output)
    rss = rss(feature, output, intercept, slope)
    {intercept, slope, rss}
  end

  def linear_regression(feature, output) do
    feature_sum = feature |> Enum.sum()
    length = feature |> length()
    feature_avg = feature_sum / length
    output_sum = output |> Enum.sum()
    output_avg = output_sum / length

    {feature_squared, feature_output} = 0..(length-1)
    |> Enum.reduce({0, 0}, fn(i, acc) ->
      f = Enum.at(feature, i)
      o = Enum.at(output, i)
      {acc_f, acc_o} = acc
      {acc_f + f*f, acc_o + f*o}
    end)

    numerator = feature_output - (feature_sum * output_sum) / length
    denominator = feature_squared - (feature_sum * feature_sum) / length

    slope = numerator / denominator
    intercept = output_avg - slope * feature_avg

    {intercept, slope}
  end

  def rss(feature, output, intercept, slope) do
    length = feature |> length()

    0..(length-1)
    |> Enum.reduce(0, fn(i, acc) ->
      f = Enum.at(feature, i)
      o = Enum.at(output, i)
      prediction = intercept + f * slope
      diff = o - prediction
      acc + diff * diff
    end)
  end
end
