defmodule Regression do
  use Tensor
  @moduledoc """
  Documentation for Regression.
  """

  @doc """
  Loads data to Regression with selecting feature and output columns.

  ## Examples

      iex> Regression.load_data("test/data/kc_house_sample_data.csv", ["price", "sqft_living", "bedrooms"])
      {[2.219e5, 5.38e5, 1.8e5, 6.04e5, 5.1e5, 1.225e6, 2.575e5, 291850.0, 2.295e5, 3.23e5],
       [
         [1180.0, 2570.0, 770.0, 1960.0, 1680.0, 5420.0, 1715.0, 1060.0, 1780.0,
          1890.0],
         [3.0, 3.0, 2.0, 4.0, 3.0, 4.0, 3.0, 3.0, 3.0, 3.0]
       ]}
  """
  def load_data(file_name, columns) do
    list = file_name
    |> File.stream!
    |> CSV.decode(headers: true)
    |> Enum.map(fn {:ok, item} -> Enum.map(columns, fn column -> {val, ""} = Float.parse(item[column]); val end) end)

    full_matrix = Matrix.new(list, length(list), length(columns))

    [out_v | features_v] = Matrix.columns(full_matrix)

    out = out_v |> Vector.to_list
    features = features_v |> Enum.map(fn v -> Vector.to_list(v) end)
    {out, features}
  end
end
