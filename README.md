# Elixir Machine Learning

This is collection of Machine Learning algorithms implemented in Elixir. Primarily for me to practice understanding them.

## Regression

Implemented algorithms are listed below.

### Linear regression
Algorithm to calculate intercept, slope and residual sum of squares for linear regression.

```
Regression.Linear.new("test/data/kc_house_train_data.csv", "test/data/kc_house_test_data.csv", ["sqft_living"], "price")

  {-13816.056604250334, 225.66344899088656, 105046133759.27667}

```

# Tests

Just run `mix test` in specific elixir folder.